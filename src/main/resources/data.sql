INSERT INTO provider(id, name, address, business_name) VALUES(1, 'John Smith', '11 Sur', 'Apple');
INSERT INTO provider(id, name, address, business_name) VALUES(2, 'Anne Mitchel', '2 Sur', 'Amazon');
INSERT INTO provider(id, name, address, business_name) VALUES(3, 'Peter Snow', '11 Norte', 'Google');
INSERT INTO provider(id, name, address, business_name) VALUES(4, 'Jeff Donovan', '11 Sur', 'Tesla');
INSERT INTO provider(id, name, address, business_name) VALUES(5, 'John Cena', '11 Sur', 'Walmart');