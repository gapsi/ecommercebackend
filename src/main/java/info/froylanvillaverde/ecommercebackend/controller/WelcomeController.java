package info.froylanvillaverde.ecommercebackend.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/welcome")
@CrossOrigin(origins = "http://localhost:3000")
public class WelcomeController {

    @GetMapping("/client")
    public String getClient(){
        return "Client 01";
    }

    @GetMapping("/version")
    public String getVersion(){
        return "0.0.1";
    }
}
