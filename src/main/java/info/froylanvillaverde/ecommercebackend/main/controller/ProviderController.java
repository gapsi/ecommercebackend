package info.froylanvillaverde.ecommercebackend.main.controller;

import info.froylanvillaverde.ecommercebackend.main.dao.entity.Provider;
import info.froylanvillaverde.ecommercebackend.main.service.ProviderService;
import org.apache.catalina.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/provider")
@CrossOrigin(origins = "http://localhost:3000")
public class ProviderController {

    @Autowired
    private ProviderService providerService;

    @GetMapping("/")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<List<Provider>> getAllProviders(){

        return ResponseEntity.ok(providerService.getAllProviders());
    }

    @GetMapping("/{providerId}")
    @ResponseStatus(code = HttpStatus.OK)
    public ResponseEntity<Provider> getProviderById(@PathVariable Long providerId){
        Provider provider;

        if (providerId == -1) provider = new Provider();
        else provider = providerService.getProviderById(providerId);

        return ResponseEntity.ok(provider);
    }

    @PutMapping("/{providerId}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public ResponseEntity<Provider> updateProvider(@RequestBody Provider provider){
        Provider providerUpdated = providerService.updateProvider(provider);
        return new ResponseEntity<Provider>(providerUpdated, HttpStatus.NO_CONTENT);
    }

    @PostMapping("/")
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity saveProvider(@RequestBody Provider provider){
        try {
            providerService.saveProvider(provider);
        } catch (Exception e) {
            e.printStackTrace();
        }

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{providerId}")
                .buildAndExpand(provider.getId())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @DeleteMapping("/{providerId}")
    public ResponseEntity<Void> deleteProviderById(@PathVariable long providerId){
        providerService.deletProviderById(providerId);
        return ResponseEntity.noContent().build();
    }
}
