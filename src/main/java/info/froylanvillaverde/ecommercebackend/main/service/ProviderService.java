package info.froylanvillaverde.ecommercebackend.main.service;

import info.froylanvillaverde.ecommercebackend.main.dao.ProviderRepository;
import info.froylanvillaverde.ecommercebackend.main.dao.entity.Provider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class ProviderService {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ProviderRepository providerRepository;

    public List<Provider> getAllProviders(){
        log.debug("getAllProviders()");
        return providerRepository.findAll();
    }

    public Provider getProviderById(Long id){
        log.debug("getProviderById with id: {}", id);
        return providerRepository.findById(id).get();
    }

    public void saveProvider(Provider provider) throws Exception {
        log.debug("saveProvider: {}", provider);
        Provider provider1 = providerRepository.getProviderByName(provider.getName());

        if (!Objects.isNull(provider1))
            throw new Exception("Ya existe el proveedor");
        else
            providerRepository.save(provider);
    }

    public Provider updateProvider(Provider provider){
        log.debug("updateProvider: {}", provider);
        return providerRepository.save(provider);
    }

    public void deletProviderById(Long id){
        log.info("deletProviderById with id: {}", id);
        providerRepository.deleteById(id);
    }
}
