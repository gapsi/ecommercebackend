package info.froylanvillaverde.ecommercebackend.main.dao;

import info.froylanvillaverde.ecommercebackend.main.dao.entity.Provider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProviderRepository extends JpaRepository<Provider, Long> {

    public Provider getProviderByName(String name);
}
